#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

products=()
#products+=(IntelliJIdea)
products+=(GoLand)
products+=(IntelliJIdea)

echo '==> Removing evaluation key...'

for product in ${products[*]}; do
  rm -rf ~/.config/JetBrains/${product}*/eval
done

rm -rf ~/.java/.userPrefs/jetbrains/*

echo '==> Resetting evalsprt in other.xml...'

for product in ${products[*]}; do
  sed -i -E 's/<property name=\"evl.*\".*\/>//' ~/.config/JetBrains/${product}*/options/other.xml
done

echo 'Done.'

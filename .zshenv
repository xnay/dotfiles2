typeset -U PATH path

# Other XDG paths.
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# Fixing Paths.
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export ZDOTDIR=$HOME/.config/zsh
export HISTFILE="$XDG_CACHE_HOME"/zsh/history
export GOPATH="$XDG_DATA_HOME"/go
export GOBIN=$GOPATH/bin
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

# Path
path=("$HOME/go/bin" "$XDG_DATA_HOME/npm/bin" "$HOME/.local/bin" "$path[@]")
export PATH
